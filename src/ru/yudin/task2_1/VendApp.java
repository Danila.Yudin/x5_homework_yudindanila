package ru.yudin.task2_1;

//Переписать проект вендинговый автомат используя конструктор, перечисления и взаимодействие с пользователем.

public class VendApp {
    private double money = 0;
    private Drink[] drinks;

    public VendApp() {
    }

    public VendApp(Drink[] drinks) {
        this.drinks = drinks;
    }

    public void addMoney(double money) {
        this.money = money;
    }

    private Drink getDrink(int key) {
        if (key < drinks.length){
            return drinks[key];
        } else {
            return null;
        }
    }

    }
}
