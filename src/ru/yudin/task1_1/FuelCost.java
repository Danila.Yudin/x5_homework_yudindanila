package ru.yudin.task1_1;

//Реализовать программу, которая считает стоимость бензина
//(на вход программе передается кол-во литров, на выходе печатается стоимость).
// Пример: стоимость литра бензина 43 рубля. На вход подается 50, на выходе должно быть 2150 руб.

import java.math.BigDecimal;

public class FuelCost {
    public static void main(String[] args) {
        try {
            BigDecimal fuel_price = new BigDecimal(43);
            BigDecimal fuel_volume = new BigDecimal(args[0]);
            BigDecimal fuel_cost = fuel_price.multiply(fuel_volume);
            System.out.println("Cost is " + fuel_cost);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Необходимо ввести корректное значение.");
        } catch (NumberFormatException e) {
            System.out.println("Введенное значение неверного типа.");
        }
    }
}
