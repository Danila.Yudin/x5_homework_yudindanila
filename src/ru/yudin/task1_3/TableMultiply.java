package ru.yudin.task1_3;

//Написать программу для вывода на экран таблицы умножения до введенного пользователем порога.

public class TableMultiply {
    private static int limit;
    public static void main(String[] args) {
        try {
            limit = Integer.parseInt(args[0]);
            result(limit);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Необходимо ввести корректное значение.");
        } catch (NumberFormatException e) {
            System.out.println("Введенное значение неверного типа.");
        }
    }

    private static void result(int limit) {
        for (int i = 1; i <= limit; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.println(i + " * " + j + " = " + i * j);
            }
        }
    }
}
