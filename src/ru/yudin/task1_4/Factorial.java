package ru.yudin.task1_4;

//Написать программу, которая выводит факториал для N чисел.

public class Factorial {
    private static double NNN;

    public static void main(String[] args) {
        try {
            NNN = Double.parseDouble(args[0]);
            System.out.println(fact(NNN));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Необходимо ввести корректное значение.");
        } catch (NumberFormatException e) {
            System.out.println("Введенное значение неверного типа.");
        }
    }

    static double fact(double num) {
        if (num == 1) {
            return 1;
        }
        return num * fact(num - 1);
    }
}
